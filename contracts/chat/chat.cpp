#include<chat.hpp>

void chat::store(account_name account, string input, string output) {
    eosio::print("Storing ", input, "\n");
    kb knowledge(_self, _self);
    auto iterator = knowledge.find(eosio::string_to_name(input.c_str()));
    if (iterator == knowledge.end()) { 
        knowledge.emplace(_self, [&](auto& k) {
            k.input = eosio::string_to_name(input.c_str());
            k.output = output;
        });
    } else {
        eosio::print("Overwriting ", iterator->output, "\n");
        knowledge.modify(iterator, _self, [&](auto& k) {
            k.output = output;
        });
    }
}
   

void chat::listen(account_name account, string input) {
    kb knowledge(_self, _self);
    auto iterator = knowledge.find(eosio::string_to_name(input.c_str()));
    if (iterator == knowledge.end()) {
        eosio::print("i don't know :(");
    } else {
        eosio::print(iterator->output);
    }
}



EOSIO_ABI(chat, (listen)(store));
