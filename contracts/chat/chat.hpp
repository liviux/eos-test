#include<eosiolib/eosio.hpp>
#include<string>

using std::string;

class chat : eosio::contract {
    public:
        chat(account_name account) : eosio::contract(account) {}

        void listen(account_name account, string message);
        void store(account_name account, string input, string output);

    private:
        // @abi table know
        struct knowledge_base {
            uint64_t input;
            string output;

            auto primary_key() const { return input; }
        };

        typedef eosio::multi_index<N(knowledge), knowledge_base> kb;
};
