#include <guess.hpp>

void guess::guess_bool( account_name account, bool my_guess) {
    if (now() % 2 == my_guess) {
        eosio::print(":)");
    } else {
        eosio::print(":(");
    }
}

EOSIO_ABI(guess, (guess_bool))
