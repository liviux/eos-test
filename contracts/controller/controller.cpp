#include<controller.hpp>


void controller::talk( account_name account, string message ) {
    eosio::action(
        eosio::permission_level{account, N(active)},
        N(chat),
        N(listen),
        std::make_tuple(_self, message)
    ).send();
}

void controller::store( account_name account, string input, string output ) {
    eosio::action(
        eosio::permission_level{account, N(active)},
        N(chat),
        N(store),
        std::make_tuple(account, input, output)
    ).send();
}

void controller::guess( account_name account, bool my_guess ) {
    eosio::action(
        eosio::permission_level{account, N(active)},
        N(guess),
        N(guess_bool),
        std::make_tuple(account, my_guess)
    ).send();
}


EOSIO_ABI(controller, (talk)(store)(guess))
